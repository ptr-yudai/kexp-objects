#!/bin/sh
timeout --foreground 300 qemu-system-x86_64 \
    -m 256M \
    -kernel ./bzImage \
    -initrd ./rootfs.cpio \
    -append "root=/dev/ram rw console=ttyS0 oops=panic panic=1 nokaslr quiet" \
    -cpu qemu64 \
    -monitor /dev/null \
    -nographic -enable-kvm -gdb tcp::1234
